# Write a program that prints the numbers from 1 to 100.
# But for multiples of three print "Fizz" instead of the number
# and for the multiples of five print "Buzz". For numbers which
# are multiples of both three and five print "FizzBuzz".

def fizz_buzz
  100.times do | count |
    if (count % 3 == 0) && (count % 5 == 0)
      puts 'FizzBuzz'
    elsif (count % 3 == 0) 
      puts 'Fizz'
    elsif (count % 5 == 0)
      puts 'Buzz'
    else
      puts count
    end
  end
end

fizz_buzz