def sum_multiples_of(factors=[], limit)
  total = 0
  0.upto(limit-1) { | count |
    done = false
    factors.each do | factor |
      if (count % factor) == 0 && !done
        total += count
        done = true
      end
    end
  }
  puts "Sum of all multiples of #{factors} under #{limit}: #{total}"
end

sum_multiples_of [3, 5], 1000
