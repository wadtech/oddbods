# n! means n  (n  1)  ...  3  2  1

# For example, 10! = 10  9  ...  3  2  1 = 3628800,
# and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

# Find the sum of the digits in the number 100!

def recursive_factorial(n)
  if n.zero? 
    1 
  else
    n * recursive_factorial(n - 1)
  end
end

def sum_factorial(n)
  sum = 0
  recursive_factorial(n).to_s.each_char do | c |
    sum += c.to_i
  end
  puts sum
end

sum_factorial 100