# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.

def is_prime?(n)
  if n == 1
    return false #not a prime
  end
  2.upto(Math.sqrt(n)) do | number |
    if (n % number == 0)
      return false # not a prime
      break
    end
  end
  return true #is a prime
end

def sum_primes_below(n)
  sum = 0
  0.upto(n) do | count |
    if is_prime? count
      sum += count
      print "Sum is currently: #{sum}\r"
    end
  end
  puts "Sum of primes below #{n}: #{sum}"
end

sum_primes_below 2000000