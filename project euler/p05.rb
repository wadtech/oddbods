# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

require 'rational'

def smallest_evenly_divisible_number(range)
  num = (1..range).inject(1) { |result, n| result.lcm n }
  puts "Smallest evenly divisible number between 1-#{range} is #{ num }."
end

smallest_evenly_divisible_number 20