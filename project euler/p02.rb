def sum_of_even_fibonacci_numbers_under(number)
  total = 0
  a, b = 1, 2  
  while b < number do 
   if b % 2 ==0
    total += b
   end 
   a, b = b, a+b  
  end
  puts "Total: #{total}"  
end

sum_of_even_fibonacci_numbers_under 4000000