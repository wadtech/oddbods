# The sum of the squares of the first ten natural numbers is,

# 12 + 22 + ... + 102 = 385
# The square of the sum of the first ten natural numbers is,

# (1 + 2 + ... + 10)2 = 552 = 3025
# Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025  385 = 2640.

# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

#bonus extra short method name ha ha ha.
def difference_between_sum_of_squares_and_square_of_sum(n = 100)
  (1..n).reduce(:+)**2 - (1..n).inject(0) {|result, i| result = result + (i * i)}
end

puts difference_between_sum_of_squares_and_square_of_sum